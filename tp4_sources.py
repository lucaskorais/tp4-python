def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max=0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle=0 #longueur du plateau actuel
    for i in range(len(chaine)):
        if chaine[i]==chaine[i-1]: # si la lettre actuelle est égale à la précédente
            lg_actuelle+=1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle>lg_max:
                lg_max=lg_actuelle
            lg_actuelle=1
    if lg_actuelle>lg_max: #cas du dernier plateau
        lg_max=lg_actuelle
    return lg_max


# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

def plus_peuple(ville,popu):
    """Trouve la ville avec le plus d'habitant

    Args:
        ville (string): La liste des villes
        popu (int): la liste des nombre d'habitant de chaque ville

    Returns:
        string: Le nom de la ville la plus peuplé
    """    
    maxi=max(popu)
    for i in range(len(popu)):
            if popu[i]==maxi:
                return ville[i]

print(plus_peuple(liste_villes,population))
     
def exo3(nombre):
    res=0
    chiffre=["0","1","2","3","4","5","6","7","8","9"]
    for i in nombre:
        for u in range(len(chiffre)):
            if i==chiffre[u]:
                res=res*10+u
    return res
print(exo3("2020"))

def exo4(liste_mot,lettre):
    """fonction qui nous renvoie la liste des mot commencent par la lettre definit en parametre

    Args:
        liste_mot (string): une liste de mot
        lettre (string): la lettre a rechercher

    Returns:
        string: la liste des mot commencent par la lettre placer en parametre
    """    
    res=[]
    for i in range(len(liste_mot)):
        if liste_mot[i][0]==lettre:
            res.append(liste_mot[i])
    return res

print(exo4(["salut","hello","hallo","ciao","hola"],"h"))


def exo5(phrase):
    """ fonction qui retourne tous les mot d'une liste quyi sont alphabetique

    Args:
        phrase (string): une phrase

    Returns:
        string: la liste des mot alphabetique de la phrase
    """    
    res=[]
    mot=""
    for i in range(len(phrase)) :
        if phrase[i] == " " or phrase[i]=="’"or phrase[i]=="!":
            if mot.isalpha()==True:
                res.append(mot)
            mot=""
        else:
            mot=mot+phrase[i]
    return res
print(exo5("!Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!"))

def exo6(phrase,lettre):
    return exo4(exo5(phrase),lettre)
print(exo6("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!","C"))

def exo71(nombre):
    """Fonction qui genere un liste de taille n avec comme deux premiere valeur False et le reste True

    Args:
        nombre (int): le nombre n qui defini la taille de la liste

    Returns:
        bool: la liste de booleen souhaiter
    """    
    res=[]
    cpt=1
    i=1
    while i<=nombre:
        if cpt<=2:
            res.append(False)
        else:
            res.append(True)
        cpt+=1
        i+=1
    return res
print(exo71(5))

def exo72(liste,nombre):
    """fonction change tous les multiple du nombre en parametre en False dans la liste donner 

    Args:
        liste (bool): une liste de booleen
        nombre (int): le multiple souhaiter

    Returns:
        bool: la liste de booleen mis a jour
    """    
    for i in range(len(liste)):
        if i!=nombre:
            if (i%nombre ==0)==True:
                liste[i]=False
    return liste

print(exo72(exo71(7),2))

def exo73(nombre):
    """fonction qui sort les nombre premier possible inferieur au nombre donner en parametre

    Args:
        nombre (int): la limite a la quelle s'arrete le calcul

    Returns:
        int: la liste des nombre premiers inferieur a n
    """    
    liste=exo71(nombre)
    i=2
    res=[]
    while i<=nombre:
        liste=exo72(liste,i)
        i+=1
    for i in range(len(liste)):
        if liste[i]==True:
            res.append(i)

    return res
print(exo73(80000))
